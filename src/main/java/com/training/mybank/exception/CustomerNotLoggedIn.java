package com.training.mybank.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerNotLoggedIn extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;
	public CustomerNotLoggedIn(String message) {
		super();
		this.message = message;
	}
	
}
