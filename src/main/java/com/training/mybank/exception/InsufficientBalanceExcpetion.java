package com.training.mybank.exception;

public class InsufficientBalanceExcpetion extends RuntimeException {

	private static final long serialVersionUID = 1L;
	final String message;

	public InsufficientBalanceExcpetion(String message) {
		super(message);
		this.message = message;
	}



}
