package com.training.mybank.service;

import java.util.List;

import com.training.mybank.dto.AccountDetails;
import com.training.mybank.dto.AccountSummaryResponse;

import jakarta.validation.Valid;

public interface AccountService {

	List<AccountSummaryResponse> getAccountDetails(@Valid long customerId);

	AccountDetails retrieveTransactions(Long accountNumber);

}
