package com.training.mybank.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.training.mybank.dto.FundTransferDto;
import com.training.mybank.dto.OtpRequest;
import com.training.mybank.dto.OtpResponse;
import com.training.mybank.dto.OtpVerification;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.BeneficiaryAccount;
import com.training.mybank.entity.FundTransfer;
import com.training.mybank.entity.FundTransferStatus;
import com.training.mybank.entity.Transaction;
import com.training.mybank.entity.TransactionType;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.InsufficientBalanceExcpetion;
import com.training.mybank.exception.OtpVerificationFailed;
import com.training.mybank.feing.OtpFiegnClient;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.BeneficiaryAccountRepository;
import com.training.mybank.repository.FundTransferRepository;
import com.training.mybank.repository.TransactionRepository;
import com.training.mybank.service.FundTransferService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class FundTransferServiceImpl implements FundTransferService {

	private final AccountRepository accountRepository;
	private final FundTransferRepository fundTransferRepository;
	private final TransactionRepository transactionRepository;

	private final BeneficiaryAccountRepository beneficiaryAccountRepository;

	private final OtpFiegnClient fiegnClient;

	@Override
	public ResponseDto fundTransfer(FundTransferDto dto) {

		Account fromAccount = accountRepository.findByAccountNumber(dto.getFromAccountNumber());
		if (Objects.isNull(fromAccount)) {
			log.warn("From Account not found");
			throw new AccountNotFound("from account not found");
		}

		Account toAccount = accountRepository.findByAccountNumber(dto.getToAccountNumber());
		if (Objects.isNull(toAccount)) {
			log.warn("To Account not found");
			throw new AccountNotFound("To account not found");
		}
		List<BeneficiaryAccount> beneficiary = beneficiaryAccountRepository
				.findAllByAccountAccountId(fromAccount.getAccountId());

		List<Long> accountList = beneficiary.stream().map(t -> t.getAccountNumber()).toList();
		Long toAccountNumber = toAccount.getAccountNumber();

		if (accountList.contains(toAccountNumber)) {

		
			if (fromAccount.getAccountBalance() < dto.getAmount()) {
				log.warn("Insufficient Balance");
				throw new InsufficientBalanceExcpetion("Insufficient Balance");
			}

			OtpRequest otpRequest = new OtpRequest();
			otpRequest.setTo("k.vinay@hcl.com");
			FundTransfer fundTransfer = new FundTransfer();
			OtpResponse otp = fiegnClient.getOtp(otpRequest);
			fundTransfer.setOtpToken(otp.getOtp());
			fundTransfer.setAccountNumber(fromAccount.getAccountNumber());
			fundTransfer.setBeneficiaryAccountNumber(toAccount.getAccountNumber());
			fundTransfer.setFundTransferStatus(FundTransferStatus.PENDING);
			fundTransfer.setRemarks("fund transfer");
			fundTransfer.setTransactionAmount(dto.getAmount());
			fundTransfer.setTransactionType(dto.getTransactionType());
			fundTransferRepository.save(fundTransfer);
		}

		return new ResponseDto("Validate the OTP", 200);

	}

	@Override
	public ResponseDto validateOtp(OtpVerification otpVerification) {

		FundTransfer fundTransfer = fundTransferRepository.findByFundTransferId(otpVerification.getFundTransferId());
		if (!otpVerification.getOtp().equals(fundTransfer.getOtpToken()))
			throw new OtpVerificationFailed("Invalid Otp,Verification failed");

		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();
		Account fromAccount = accountRepository.findByAccountNumber(fundTransfer.getAccountNumber());
		Account toAccount = accountRepository.findByAccountNumber(fundTransfer.getBeneficiaryAccountNumber());

		switch (fundTransfer.getTransactionType()) {
		case DEBIT:
			fromAccount.setAccountBalance(fromAccount.getAccountBalance() - fundTransfer.getTransactionAmount());
			toAccount.setAccountBalance(toAccount.getAccountBalance() + fundTransfer.getTransactionAmount());

			transaction1.setAccountNumber(fromAccount.getAccountNumber());
			transaction1.setTransactionAmount(fundTransfer.getTransactionAmount());
			transaction1.setTransactionDate(LocalDateTime.now());
			transaction1.setTransactionType(TransactionType.DEBIT);
			transaction1.setFundTransfer(fundTransfer);

			transaction2.setAccountNumber(toAccount.getAccountNumber());
			transaction2.setTransactionAmount(fundTransfer.getTransactionAmount());
			transaction2.setTransactionDate(LocalDateTime.now());
			transaction2.setTransactionType(TransactionType.CREDIT);
			transaction2.setFundTransfer(fundTransfer);

			break;

		case CREDIT:
			toAccount.setAccountBalance(toAccount.getAccountBalance() - fundTransfer.getTransactionAmount());
			fromAccount.setAccountBalance(fromAccount.getAccountBalance() + fundTransfer.getTransactionAmount());

			transaction1.setAccountNumber(toAccount.getAccountNumber());
			transaction1.setTransactionAmount(fundTransfer.getTransactionAmount());
			transaction1.setTransactionDate(LocalDateTime.now());
			transaction1.setTransactionType(TransactionType.DEBIT);
			transaction1.setFundTransfer(fundTransfer);

			transaction2.setAccountNumber(fromAccount.getAccountNumber());
			transaction2.setTransactionAmount(fundTransfer.getTransactionAmount());
			transaction2.setTransactionDate(LocalDateTime.now());
			transaction2.setTransactionType(TransactionType.CREDIT);
			transaction2.setFundTransfer(fundTransfer);

			break;

		default:
			throw new IllegalArgumentException("Unexpected value: " + fundTransfer.getTransactionType());
		}

		fundTransfer.setFundTransferStatus(FundTransferStatus.SUCCESS);
		transactionRepository.save(transaction1);
		transactionRepository.save(transaction2);
		accountRepository.save(toAccount);
		accountRepository.save(fromAccount);
		fundTransferRepository.save(fundTransfer);

		return new ResponseDto("Transaction Successfull", 200);
	}
}
