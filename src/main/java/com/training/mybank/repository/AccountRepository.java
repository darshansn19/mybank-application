package com.training.mybank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.mybank.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByAccountNumber(Long accountNumber);

	List<Account> findByCustomerCustomerId(long customerId);

	Account findByCustomerCustomerIdAndAccountNumber(Long customerId, Long accountNumber);

}
