package com.training.mybank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.AccountDetails;
import com.training.mybank.dto.AccountSummaryResponse;
import com.training.mybank.dto.TransactionResponseDto;
import com.training.mybank.entity.AccountType;
import com.training.mybank.entity.TransactionType;
import com.training.mybank.service.impl.AccountServiceImpl;

@ExtendWith(SpringExtension.class)
class AccountControllerTest {

	@Mock
	AccountServiceImpl accountServiceImpl;
	
	@InjectMocks
	AccountController accountController;
	
	@Test
	void testGetAccountDetails() {
		List<AccountSummaryResponse> responses = new ArrayList<>();
 
		AccountSummaryResponse accountSummaryResponse = new AccountSummaryResponse("darshan", AccountType.CURRENT,
				100.00, 12334l);
		AccountSummaryResponse accountSummaryResponse2 = new AccountSummaryResponse("darshan", AccountType.SAVINGS,
				100.00, 5645645l);
		responses.add(accountSummaryResponse2);
		responses.add(accountSummaryResponse);
		Mockito.when(accountServiceImpl.getAccountDetails(anyLong())).thenReturn(responses);
		ResponseEntity<List<AccountSummaryResponse>> result = accountController.getAccountDetails(1L);
		assertEquals(responses.size(), result.getBody().size());
 
	}
 
	@Test
	void testRetreiveTopTransactions() {
		List<TransactionResponseDto> dtos = new ArrayList<>();
		TransactionResponseDto dto1 = new TransactionResponseDto();
		dto1.setAccountNumber(12333l);
		dto1.setTransactionAmount(100.00);
		dto1.setTransactionType(TransactionType.CREDIT);
 
		TransactionResponseDto dto2 = new TransactionResponseDto();
		dto2.setAccountNumber(2334l);
		dto2.setTransactionAmount(100.00);
		dto2.setTransactionType(TransactionType.DEBIT);
		dtos.add(dto1);
		dtos.add(dto2);
 
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountNumber(12323232l);
		accountDetails.setAccountbalance(1000.00);
		accountDetails.setTransactionList(dtos);
 
		Mockito.when(accountServiceImpl.retrieveTransactions(anyLong())).thenReturn(accountDetails);
		ResponseEntity<AccountDetails> result = accountController.retreiveTopTransactions(12323232l);
 
		assertNotNull(result);
	}
}
